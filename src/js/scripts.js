$(function () {

    if ($(window).width() > 1499) {
        $('.menu-item-has-children').hover(
            function () {
                $('.bottom_head').css('background-color', '#faf2f6');
            },
            function () {
                $('.bottom_head').css('background-color', 'transparent');
            }
        )
    }
    ;

    $('.js_main_slider').slick({
        dots: true,
        arrows: false
    });

    $('.js_input').styler({
        onFormStyled: function () {
            $('input.js_input').each(function () {
                $(this).closest('.jq-radio').attr("style", $(this).attr("style"));
            })
        }
    });

    $('.fav_btn_block .jq-checkbox__div').html('<i class="fa fa-heart" aria-hidden="true"></i>')

    $('.js_watched_slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        prevArrow: "<i class='fa fa-angle-left back' aria-hidden='true'></i>",
        nextArrow: "<i class='fa fa-angle-right fwd' aria-hidden='true'></i>",
        responsive: [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 2,
                infinite: true
            }
        }, {
            breakpoint: 769,
            settings: {
                slidesToShow: 1,
            }
        }]
    });

    $('.jq-number__spin.plus').html('<i class=\'fa fa-angle-up back\' aria-hidden=\'true\'></i>');
    $('.jq-number__spin.minus').html('<i class=\'fa fa-angle-down back\' aria-hidden=\'true\'></i>');
    $('.jq-number__field').append('<span>шт.</span>');

    $('.js_mobile_menu').click(function () {
        $('header .top_head').toggleClass('active');
        if ($('header .top_head').hasClass('active')) {
            $('body').prepend('<div class="bg_overlay"></div>');
        } else {
            $('.bg_overlay').remove();
        }
    });

    $(document).mouseup(function (e) {
        var container = $("header .top_head");
        if (container.has(e.target).length === 0) {
            container.removeClass('active');
            $('.bg_overlay').remove();
        }
    });

    $('.js_popup').magnificPopup({
        closeBtnInside: true
    });

    $("#reg_form").validate({
        // правила
        rules: {
            name: {
                required: true, // поле обязательное для заполнения
                minlength: 2 // Минимальное число символов - 2
            },
            email: {
                required: true, // поле обязательное для заполнения
                email: true    // Пожалуйста, введите действительный адрес электронной почты
            },
            pass: {
                required: true, // поле обязательное для заполнения
                minlength: 6, // Минимальное число символов - 6
                maxlength: 20 // Максимальное число символов -20
            },
            repass: {
                required: true, // поле обязательное для заполнения
                equalTo: "#pass"
            }
        },
        // сообщение
        messages: {

            name: {
                required: " <p class='err'>Это поле обязательно для заполнения!</p>",
                minlength: "<p class='err'>Имя должно быть не менее 2 символов!</p>",
                maxlength: "<p class='err'>Имя должно быть не более 20 символа!</p>"
            },
            email: {
                required: "<p class='err'>Это поле обязательно для заполнения!</p>",
                email: "<p class='err'>Введите корректный адрес электронной почты!</p>"
            },
            pass: {
                required: "<p class='err'> Это поле обязательно для заполнения!</p>",
                minlength: "<p class='err'>Пароль должен быть не менее 6 символов!</p>",
                maxlength: "<p class='err'>Пароль должно быть не более 20 символов!</p>"
            },
            repass: {
                required: "<p class='err'>Это поле обязательно для заполнения</p>",
                equalTo: "<p class='err'>Повторите пароль!</p>"
            }

        }

    });

    $("#log_form").validate({
        // правила
        rules: {
            email: {
                required: true, // поле обязательное для заполнения
                email: true    // Пожалуйста, введите действительный адрес электронной почты
            },
            pass: {
                required: true, // поле обязательное для заполнения
                minlength: 6, // Минимальное число символов - 6
                maxlength: 20 // Максимальное число символов -20
            }
        },
        // сообщение
        messages: {

            email: {
                required: "<p class='err'>Это поле обязательно для заполнения!</p>",
                email: "<p class='err'>Введите корректный адрес электронной почты!</p>"
            },
            pass: {
                required: "<p class='err'> Это поле обязательно для заполнения!</p>",
                minlength: "<p class='err'>Пароль должен быть не менее 6 символов!</p>",
                maxlength: "<p class='err'>Пароль должно быть не более 20 символов!</p>"
            }

        }

    });

    $("#order").validate({
        // правила
        rules: {
            order_city: {
                required: true, // поле обязательное для заполнения
            },
            order_mail: {
                required: true, // поле обязательное для заполнения
                email: true    // Пожалуйста, введите действительный адрес электронной почты
            },
            order_address: {
                required: true, // поле обязательное для заполнения
            },
            order_recipient: {
                required: true, // поле обязательное для заполнения
            },
            order_tel: {
                required: true, // поле обязательное для заполнения
                phoneRUS: true
            }
        },
        // сообщение
        messages: {

            order_city: {
                required: " <p class='err'>Это поле обязательно для заполнения!</p>"
            },
            order_mail: {
                required: "<p class='err'>Это поле обязательно для заполнения!</p>",
                email: "<p class='err'>Введите корректный адрес электронной почты!</p>"
            },
            order_address: {
                required: "<p class='err'> Это поле обязательно для заполнения!</p>"
            },
            order_recipient: {
                required: "<p class='err'>Это поле обязательно для заполнения</p>"
            },
            order_tel: {
                required: "<p class='err'>Это поле обязательно для заполнения</p>"
            }

        }

    });

    $("#buisnes").validate({
        // правила
        rules: {
            buisnes_fio: {
                required: true, // поле обязательное для заполнения
            },
            buisnes_mail: {
                required: true, // поле обязательное для заполнения
                email: true    // Пожалуйста, введите действительный адрес электронной почты
            },
            buisnes_country: {
                required: true, // поле обязательное для заполнения
            },
            buisnes_city: {
                required: true, // поле обязательное для заполнения
            },
            buisnes_tel: {
                required: true, // поле обязательное для заполнения
                phoneRUS: true
            }
        },
        // сообщение
        messages: {

            buisnes_fio: {
                required: " <p class='err'>Это поле обязательно для заполнения!</p>"
            },
            buisnes_mail: {
                required: "<p class='err'>Это поле обязательно для заполнения!</p>",
                email: "<p class='err'>Введите корректный адрес электронной почты!</p>"
            },
            buisnes_country: {
                required: "<p class='err'> Это поле обязательно для заполнения!</p>"
            },
            buisnes_city: {
                required: "<p class='err'>Это поле обязательно для заполнения</p>"
            },
            buisnes_tel: {
                required: "<p class='err'>Это поле обязательно для заполнения</p>"
            }

        }

    });

    jQuery.validator.addMethod("phoneRUS", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, "Пожалуйста, введите номер телефона.");

    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function () {
        $('body,html').animate({scrollTop: 0}, 800);
    });

    $('.js_basket_delete').click(function () {
        $(this).closest('.one_product_wrap').remove();
    });

    if ($(window).width() > 1499) {
        var product_count = $('.favourites_section .main_shop_item_wrap').length,
            product_count_rest = product_count % 4,
            product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
        if (product_count_rest == 0) {
            var product_count_rest = 4,
                product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
            $(product_count_3).css({'border': 'none'})
        }
        $(product_count_3).css({'border': 'none'})
    }
    ;

    if ($(window).width() <= 1499 && $(window).width() > 1199) {
        var product_count = $('.favourites_section .main_shop_item_wrap').length,
            product_count_rest = product_count % 3,
            product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
        if (product_count_rest == 0) {
            var product_count_rest = 3,
                product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
            $(product_count_3).css({'border': 'none'})
        } else {
            $(product_count_3).css({'border': 'none'})
        }
    }
    if ($(window).width() <= 1199 && $(window).width() > 768) {
        var product_count = $('.favourites_section .main_shop_item_wrap').length,
            product_count_rest = product_count % 2,
            product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
        if (product_count_rest == 0) {
            var product_count_rest = 2,
                product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
            $(product_count_3).css({'border': 'none'})
        } else {
            $(product_count_3).css({'border': 'none'})
        }
    }
    if ($(window).width() <= 768) {
        var product_count = $('.favourites_section .main_shop_item_wrap').length,
            product_count_rest = product_count % 1,
            product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
        if (product_count_rest == 0) {
            var product_count_rest = 1,
                product_count_3 = $('.favourites_section .main_shop_item_wrap').slice(product_count - product_count_rest);
            $(product_count_3).css({'border': 'none'})
        } else {
            $(product_count_3).css({'border': 'none'})
        }
    }
    ;

    $('.accordion').on('click', '.accordion_header', function () {
        $(this).toggleClass('active').next().slideToggle();
    });

    $('.btn_filter_more').on('click', function () {
        $(this).hide().closest('.accordion_body').find('.hidden_checkbox').show(200);
    });

    $('.colored_filter li input').change(function () {
        $(this).parent().toggleClass('checked');
    });

    $('.sidebar_trigger').click(function () {
        $('aside').addClass('active');
    });
    $(document).mouseup(function (e) {
        var container = $("aside.active");
        if (container.has(e.target).length === 0) {
            container.removeClass('active');
        }
    });

});